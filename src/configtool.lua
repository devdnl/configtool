--[[============================================================================
@file    configtool.lua

@author  Daniel Zorychta

@brief   This file is the main file of the configuration tool

@note    Copyright (C) 2015 Daniel Zorychta <daniel.zorychta@gmail.com>

         This program is free software; you can redistribute it and/or modify
         it under the terms of the GNU General Public License as published by
         the  Free Software  Foundation;  either version 2 of the License, or
         any later version.

         This  program  is  distributed  in the hope that  it will be useful,
         but  WITHOUT  ANY  WARRANTY;  without  even  the implied warranty of
         MERCHANTABILITY  or  FITNESS  FOR  A  PARTICULAR  PURPOSE.  See  the
         GNU General Public License for more details.

         You  should  have received a copy  of the GNU General Public License
         along  with  this  program;  if not,  write  to  the  Free  Software
         Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.


==============================================================================]]

os.setlocale('C')
package.path  = package.path..";./src/?.lua"

--==============================================================================
-- EXTERNAL MODULES
--==============================================================================
require("wx")
require("parser")
require("utils")
require("imexcfg")
require("about")

--==============================================================================
-- PUBLIC OBJECTS
--==============================================================================
ui = {}

icon = {}
icon.px16 = {}
icon.px22 = {}
icon.px16.application_exit      = wx.wxBitmap("pixmaps/16x16/application-exit.png")
icon.px22.application_exit      = wx.wxBitmap("pixmaps/22x22/application-exit.png")
icon.px16.document_save         = wx.wxBitmap("pixmaps/16x16/document-save.png")
icon.px16.document_save_dimmed  = wx.wxBitmap("pixmaps/16x16/document-save-dimmed.png")
icon.px22.document_save         = wx.wxBitmap("pixmaps/22x22/document-save.png")
icon.px22.document_save_dimmed  = wx.wxBitmap("pixmaps/22x22/document-save-dimmed.png")
icon.px16.document_info         = wx.wxBitmap("pixmaps/16x16/documentinfo.png")
icon.px22.document_info         = wx.wxBitmap("pixmaps/22x22/documentinfo.png")
icon.px16.document_export       = wx.wxBitmap("pixmaps/16x16/document-export.png")
icon.px22.document_export       = wx.wxBitmap("pixmaps/22x22/document-export.png")
icon.px16.document_import       = wx.wxBitmap("pixmaps/16x16/document-import.png")
icon.px22.document_import       = wx.wxBitmap("pixmaps/22x22/document-import.png")
icon.px16.tools_report_bug      = wx.wxBitmap("pixmaps/16x16/tools-report-bug.png")
icon.px22.tools_report_bug      = wx.wxBitmap("pixmaps/22x22/tools-report-bug.png")
icon.px16.executable            = wx.wxBitmap("pixmaps/16x16/application-x-executable.png")
icon.px22.go_back               = wx.wxBitmap("pixmaps/22x22/go-previous.png")
icon.px22.go_back_dimmed        = wx.wxBitmap("pixmaps/22x22/go-previous-dimmed.png")
icon.px16.document_open         = wx.wxBitmap("pixmaps/16x16/document-open.png")
icon.px22.document_open         = wx.wxBitmap("pixmaps/22x22/document-open.png")

--==============================================================================
-- LOCAL OBJECTS
--==============================================================================
local ID = {}
ID.MENU_OPEN                = wx.wxID_OPEN
ID.MENU_SAVE                = wx.wxID_SAVE
ID.MENU_IMPORT              = wx.wxNewId()
ID.MENU_EXPORT              = wx.wxNewId()
ID.MENU_EXIT                = wx.wxID_EXIT
ID.MENU_HELP_ABOUT          = wx.wxID_ABOUT
ID.MENU_HELP_REPORT_BUG     = wx.wxNewId()
ID.TOOLBAR_OPEN             = wx.wxNewId()
ID.TOOLBAR_SAVE             = wx.wxNewId()
ID.TOOLBAR_IMPORT           = wx.wxNewId()
ID.TOOLBAR_EXPORT           = wx.wxNewId()

local PLUGINS_DIR = "./plugins"
local plugin      = {}

--==============================================================================
-- LOCAL FUNCTIONS
--==============================================================================
--------------------------------------------------------------------------------
-- @brief  Signal is called when main window is closing
-- @param  event
-- @return None
--------------------------------------------------------------------------------
local function EventWindowClose(event)
    if ui.Parser:IsModified() == true then
        local answer = ShowQuestionMessage(MAIN_WINDOW_NAME,
                                           SAVE_QUESTION,
                                           (wx.wxYES_NO + wx.wxCANCEL),
                                           ui.Frame)
        if answer == wx.wxID_CANCEL then
            event:Veto()
            return
        elseif answer == wx.wxID_YES then
            ui.Parser:Save()
        end
    end

    ui.Frame:Destroy()
end

--------------------------------------------------------------------------------
-- @brief  Signal is called when menu's open item is clicked
-- @param  None
-- @return None
--------------------------------------------------------------------------------
local function EventOpenConfiguration()
    local dialog = wx.wxFileDialog(ui.Frame,
                                   "Select configuration file",
                                   GetConfigPath(),
                                   "",
                                   "C/C++ header (*.h)|*.h",
                                   wx.wxFD_OPEN)

    if (dialog:ShowModal() == wx.wxID_OK) then
        arg[1] = wx.wxDos2UnixFilename(dialog:GetPath())
        SetConfigPath(dirname(arg[1]))

        if ui.Parser ~= nil then
            ui.Parser:Parse(basename(arg[1]))
            ui.Frame:SetStatusText("Configuration loaded.", 1)
        end
        return true
    else
        return false
    end
end

--------------------------------------------------------------------------------
-- @brief  Signal is called when menu's save item is clicked
-- @param  None
-- @return None
--------------------------------------------------------------------------------
local function EventSaveConfiguration()
    if ui.Parser:IsModified() == true then
        if ui.Parser:Save() == true then
            ui.Frame:SetStatusText("Configuration saved.", 1)
        end
    end
end

--------------------------------------------------------------------------------
-- @brief  Signal is called when menu's import item is clicked
-- @param  None
-- @return None
--------------------------------------------------------------------------------
local function EventImportConfiguration()
    if ui.Parser:IsParsed() then
        local dialog = wx.wxFileDialog(ui.Frame,
                                       "Import configuration file",
                                       GetConfigPath(),
                                       "",
                                       "dnx RTOS configuration files (*.dnxc)|*.dnxc",
                                       wx.wxFD_OPEN)

        if (dialog:ShowModal() == wx.wxID_OK) then
            local path = wx.wxDos2UnixFilename(dialog:GetPath())
            ImportConfiguration(path, ui.Parser)
            ui.Parser:Parse(basename(arg[1]))
            ui.Frame:SetStatusText("Configuration imported.", 1)
        end
    end
end

--------------------------------------------------------------------------------
-- @brief  Signal is called when menu's export item is clicked
-- @param  None
-- @return None
--------------------------------------------------------------------------------
local function EventExportConfiguration()
    if ui.Parser:IsParsed() then
        local dialog = wx.wxFileDialog(ui.Frame,
                                       "Export configuration file",
                                       GetConfigPath(),
                                       "",
                                       "dnx RTOS configuration files (*.dnxc)|*.dnxc",
                                       (wx.wxFD_SAVE + wx.wxFD_OVERWRITE_PROMPT))

        if (dialog:ShowModal() == wx.wxID_OK) then
            local path = dialog:GetPath()
            if not path:match("%.dnxc$") then path = path..".dnxc" end
            ExportConfiguration(path, ui.Parser)
            ui.Frame:SetStatusText("Configuration exported.", 1)
        end
    end
end

--------------------------------------------------------------------------------
-- @brief  Signal is called when About is clicked
-- @param  None
-- @return None
--------------------------------------------------------------------------------
local function EventShowAboutDialog()
    ShowAboutWindow(ui.Frame)
end

--------------------------------------------------------------------------------
-- @brief  Call function in safe mode
-- @param  func     function to call
-- @return None
--------------------------------------------------------------------------------
local function CallFunction(func)
    local status, err = pcall(func)
    if err ~= nil then
        ShowInfoMessage("Plugin error",
                        err..".",
                        ui.Frame)
    end
end

--------------------------------------------------------------------------------
-- @brief  Create main window
-- @param  None
-- @return None
--------------------------------------------------------------------------------
local function CreateMainWindow()
    ui.Frame = wx.wxFrame(wx.NULL,
                          wx.wxID_ANY,
                          MAIN_WINDOW_NAME,
                          wx.wxDefaultPosition,
                          MAIN_WINDOW_SIZE,
                          wx.wxDEFAULT_FRAME_STYLE)

    icons = wx.wxIconBundle()
    icons:AddIcon("pixmaps/16x16/view-pim-tasks.png", wx.wxBITMAP_TYPE_PNG)
    icons:AddIcon("pixmaps/22x22/view-pim-tasks.png", wx.wxBITMAP_TYPE_PNG)

    ui.Frame:SetIcons(icons)
    ui.Frame:Connect(wx.wxEVT_CLOSE_WINDOW, EventWindowClose)
end

--------------------------------------------------------------------------------
-- @brief  Create 'Configuration' menu
-- @param  None
-- @return Menu object
--------------------------------------------------------------------------------
local function CreateConfigurationMenu()
    local menu     = wx.wxMenu()
    local MenuItem = nil

    MenuItem = wx.wxMenuItem(menu, ID.MENU_OPEN, "&Open\tCtrl-O", "Open configuration")
    MenuItem:SetBitmap(icon.px16.document_open)
    menu:Append(MenuItem)
    ui.Frame:Connect(ID.MENU_OPEN,wx.wxEVT_COMMAND_MENU_SELECTED, EventOpenConfiguration)

    MenuItem = wx.wxMenuItem(menu, ID.MENU_SAVE, "&Save\tCtrl-S", "Save currently selected configuration")
    MenuItem:SetBitmap(icon.px16.document_save)
    menu:Append(MenuItem)
    ui.Frame:Connect(ID.MENU_SAVE,wx.wxEVT_COMMAND_MENU_SELECTED, EventSaveConfiguration)

    MenuItem = wx.wxMenuItem(menu, ID.MENU_IMPORT, "&Import\tCtrl-I", "Import configuration from file")
    MenuItem:SetBitmap(icon.px16.document_import)
    menu:Append(MenuItem)
    ui.Frame:Connect(ID.MENU_IMPORT, wx.wxEVT_COMMAND_MENU_SELECTED, EventImportConfiguration)

    MenuItem = wx.wxMenuItem(menu, ID.MENU_EXPORT, "&Export\tCtrl-E", "Export configuration to file")
    MenuItem:SetBitmap(icon.px16.document_export)
    menu:Append(MenuItem)
    ui.Frame:Connect(ID.MENU_EXPORT, wx.wxEVT_COMMAND_MENU_SELECTED, EventExportConfiguration)

    MenuItem = wx.wxMenuItem(menu, ID.MENU_EXIT, "&Quit\tCtrl-Q", "Quit from Configtool")
    MenuItem:SetBitmap(icon.px16.application_exit)
    menu:Append(MenuItem)
    ui.Frame:Connect(ID.MENU_EXIT, wx.wxEVT_COMMAND_MENU_SELECTED, EventWindowClose)

    return menu
end

--------------------------------------------------------------------------------
-- @brief  Create 'Help' menu
-- @param  None
-- @return Menu object
--------------------------------------------------------------------------------
local function CreateHelpMenu()
    local menu     = wx.wxMenu()
    local MenuItem = nil

    MenuItem = wx.wxMenuItem(menu, ID.MENU_HELP_REPORT_BUG, "&Report Bug...", "Report a bug...")
    MenuItem:SetBitmap(icon.px16.tools_report_bug)
    menu:Append(MenuItem)
    ui.Frame:Connect(ID.MENU_HELP_REPORT_BUG, wx.wxEVT_COMMAND_MENU_SELECTED,
        function()
            wx.wxLaunchDefaultBrowser("https://bitbucket.org/devdnl/configtool/issues/new")
        end)

    MenuItem = wx.wxMenuItem(menu, ID.MENU_HELP_ABOUT, "&About", "The Configtool information")
    MenuItem:SetBitmap(icon.px16.document_info)
    menu:Append(MenuItem)

    ui.Frame:Connect(ID.MENU_HELP_ABOUT, wx.wxEVT_COMMAND_MENU_SELECTED, EventShowAboutDialog)

    return menu
end

--------------------------------------------------------------------------------
-- @brief  Create menubar
-- @param  None
-- @return None
--------------------------------------------------------------------------------
local function CreateMenuBar()
    local menubar = wx.wxMenuBar()

    menubar:Append(CreateConfigurationMenu(), "&Configuration")

    local menuPlugins = wx.wxMenu()
    for i = 1, #plugin do
        CallFunction(
            function()
                plugin[i].IF.AddMenuItem(
                    function(name, tip, icon, func)
                        local ID       = wx.wxNewId()
                        local MenuItem = wx.wxMenuItem(menuPlugins, ID, name, tip)

                        if type(icon) == "string" and icon ~= "" then
                            MenuItem:SetBitmap(wx.wxBitmap(icon))
                        end

                        menuPlugins:Append(MenuItem)
                        ui.Frame:Connect(ID, wx.wxEVT_COMMAND_MENU_SELECTED, func)
                    end)
            end)
    end

    if menuPlugins:GetMenuItemCount() > 0 then
        menubar:Append(menuPlugins, "&Plugins")
    end

    menubar:Append(CreateHelpMenu(), "&Help")

    ui.Frame:SetMenuBar(menubar)
end

--------------------------------------------------------------------------------
-- @brief  Create menubar
-- @param  None
-- @return None
--------------------------------------------------------------------------------
local function CreateToolBar()
    ui.Toolbar = ui.Frame:CreateToolBar(wx.wxTB_TEXT)
    ui.Toolbar:SetToolBitmapSize(wx.wxSize(22, 22))

    ui.Toolbar:AddTool(ID.TOOLBAR_OPEN, "Open", icon.px22.document_open, icon.px22.document_open, wx.wxITEM_NORMAL, "Open configuration")
    ui.Frame:Connect(ID.TOOLBAR_OPEN, wx.wxEVT_COMMAND_MENU_SELECTED, EventOpenConfiguration)

    ui.Toolbar:AddTool(ID.TOOLBAR_SAVE, "Save", icon.px22.document_save, icon.px22.document_save_dimmed, wx.wxITEM_NORMAL, "Save currently selected configuration")
    ui.Frame:Connect(ID.TOOLBAR_SAVE, wx.wxEVT_COMMAND_MENU_SELECTED, EventSaveConfiguration)

    ui.Toolbar:AddSeparator()

    ui.Toolbar:AddTool(ID.TOOLBAR_IMPORT, "Import", icon.px22.document_import, "Import configuration from the file")
    ui.Frame:Connect(ID.TOOLBAR_IMPORT,wx.wxEVT_COMMAND_MENU_SELECTED, EventImportConfiguration)

    ui.Toolbar:AddTool(ID.TOOLBAR_EXPORT, "Export", icon.px22.document_export, "Export configuration to the file")
    ui.Frame:Connect(ID.TOOLBAR_EXPORT,wx.wxEVT_COMMAND_MENU_SELECTED, EventExportConfiguration)

    ui.Toolbar:AddSeparator()

    for i = 1, #plugin do
        CallFunction(
            function()
                plugin[i].IF.AddToolbarItem(
                    function(name, tip, iconPath, func)
                        local ID = wx.wxNewId()
                        ui.Toolbar:AddTool(ID, name, wx.wxBitmap(iconPath), tip)
                        ui.Frame:Connect(ID, wx.wxEVT_COMMAND_MENU_SELECTED, func)
                    end)
            end)
    end

    ui.Toolbar:Realize()
end

--------------------------------------------------------------------------------
-- @brief  Create StatusBar
-- @param  None
-- @return None
--------------------------------------------------------------------------------
local function CreateStatusBar()
    ui.statusBar = ui.Frame:CreateStatusBar(2)
    ui.Frame:SetStatusText("Welcome to Configtool!", 0)
end

--------------------------------------------------------------------------------
-- @brief  Create Parser window
-- @param  None
-- @return None
--------------------------------------------------------------------------------
local function CreateParserWindow()
    ui.Parser = CreateParser(ui.Frame)
    if type(arg[1]) == "string" then
        ui.Parser:Parse(basename(arg[1]))
    end
end

--------------------------------------------------------------------------------
-- @brief  Create StatusBar
-- @param  None
-- @return None
--------------------------------------------------------------------------------
local function LoadPlugins()
    local plugins, files = wx.wxDir().GetAllFiles(PLUGINS_DIR, "*.lua")
    for i, name in ipairs(files) do
        name = wx.wxDos2UnixFilename(name)

        local f, err = loadfile(name)
        if err == nil then
            plugin[i]      = {}
            plugin[i].IF   = f()
            plugin[i].name = name:gsub(PLUGINS_DIR, "")
            CallFunction(plugin[i].IF.Loaded)
        else
            ShowInfoMessage("Plugin error",
                            "Plugin '"..name.."' will not be enabled.\n"..err..".",
                            ui.Frame)
        end
    end
end

--------------------------------------------------------------------------------
-- @brief  Function create widgets
-- @param  None
-- @return None
--------------------------------------------------------------------------------
local function CheckArguments()
    for i = 1, #arg do
        if arg[i]:match("help") then
            print("Usage: configtool [options]")
            print("")
            print("  PLUGINS-DIR=<file>         set plugin directory")
            print("  import=<file>:<configdir>  import selected configuration")
            print("  export=<file>:<configdir>  export selected configuraiton")
            os.exit(1)

        elseif arg[i]:match("PLUGINS%-DIR%=.*") then
            local path  = wx.wxDos2UnixFilename(arg[i]:match("PLUGINS%-DIR%=(.*)"))
            PLUGINS_DIR = path

        elseif arg[i]:match("import%=.*") then
            ui.Frame = nil

            local arg = wx.wxDos2UnixFilename(arg[i]:match("import%=(.*)"))
            local path, cwd = arg:match("^(.*):(.*)$")

            SetConfigPath(cwd.."/")
            ImportConfiguration(path, CreateParser())
            print("Configuration imported.")
            os.exit(0)

        elseif arg[i]:match("export%=.*") then
            ui.Frame = nil

            local arg = wx.wxDos2UnixFilename(arg[i]:match("export%=(.*)"))
            local path, cwd = arg:match("^(.*):(.*)$")

            SetConfigPath(cwd.."/")
            ExportConfiguration(path, CreateParser())
            print("Configuration exported.")
            os.exit(0)
        else
            SetConfigPath(dirname(arg[i]))
        end
    end
end

--------------------------------------------------------------------------------
-- @brief  Function create widgets
-- @param  None
-- @return None
--------------------------------------------------------------------------------
local function main()
    CreateMainWindow()
    CheckArguments()
    LoadPlugins()
    CreateMenuBar()
    CreateToolBar()
    CreateStatusBar()
    CreateParserWindow()

    ui.Frame:Show(true)
    wx.wxGetApp():MainLoop()
end

main()
