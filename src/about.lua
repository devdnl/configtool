﻿--[[============================================================================
@file    about.lua

@author  Daniel Zorychta

@brief   About dialog.

@note    Copyright (C) 2015 Daniel Zorychta <daniel.zorychta@gmail.com>

         This program is free software; you can redistribute it and/or modify
         it under the terms of the GNU General Public License as published by
         the  Free Software  Foundation;  either version 2 of the License, or
         any later version.

         This  program  is  distributed  in the hope that  it will be useful,
         but  WITHOUT  ANY  WARRANTY;  without  even  the implied warranty of
         MERCHANTABILITY  or  FITNESS  FOR  A  PARTICULAR  PURPOSE.  See  the
         GNU General Public License for more details.

         You  should  have received a copy  of the GNU General Public License
         along  with  this  program;  if not,  write  to  the  Free  Software
         Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.


==============================================================================]]

--==============================================================================
-- EXTERNAL MODULES
--==============================================================================
require("wx")

--==============================================================================
-- GLOBAL OBJECTS
--==============================================================================

--==============================================================================
-- LOCAL OBJECTS
--==============================================================================
local ui = {}

local VERSION = "v1.2.2"
local URL = "http://www.dnx-rtos.org/"
local PROGRAMMERS = "Daniel Zorychta <daniel.zorychta@gmail.com>"

--==============================================================================
-- GLOBAL FUNCTIONS
--==============================================================================
--------------------------------------------------------------------------------
-- @brief  Show credits dialog
-- @param  parent       parent window
-- @return None
--------------------------------------------------------------------------------
local function ShowCreditsDialog(parent)
    local ui = {}
    local ID = {}
    ID.BUTTON_CLOSE = wx.wxNewId()

    -- create license dialog
    ui.window = wx.wxDialog(parent, wx.wxID_ANY, "Credits")
    ui.FlexGridSizer1 = wx.wxFlexGridSizer(0, 1, 0, 0)

    -- create notebook for pages
    ui.Notebook = wx.wxNotebook(ui.window, wx.wxID_ANY)

    -- create program page
    ui.Panel_program = wx.wxPanel(ui.Notebook, wx.wxID_ANY)
    ui.FlexGridSizer2 = wx.wxFlexGridSizer(0, 3, 0, 0)

        -- insert names of persons which create program
        ui.TextCtrl = wx.wxTextCtrl(ui.Panel_program,
                                    wx.wxID_ANY,
                                    PROGRAMMERS,
                                    wx.wxDefaultPosition,
                                    wx.wxSize(400,300),
                                    (wx.wxTE_MULTILINE+wx.wxTE_READONLY))

        ui.FlexGridSizer2:Add(ui.TextCtrl,
                              1,
                              (wx.wxALL+wx.wxALIGN_CENTER_HORIZONTAL+wx.wxALIGN_CENTER_VERTICAL),
                              5)

        ui.Panel_program:SetSizer(ui.FlexGridSizer2)
        ui.FlexGridSizer2:Fit(ui.Panel_program)
        ui.FlexGridSizer2:SetSizeHints(ui.Panel_program)

    -- add pages to the notebook
    ui.Notebook:AddPage(ui.Panel_program, "Program", false)

    -- add notebook to the sizer
    ui.FlexGridSizer1:Add(ui.Notebook,
                          1,
                          (wx.wxALL+wx.wxALIGN_CENTER_HORIZONTAL+wx.wxALIGN_CENTER_VERTICAL),
                          5)

    -- add close button
    ui.Button_close = wx.wxButton(ui.window, ID.BUTTON_CLOSE, "Close")

    ui.FlexGridSizer1:Add(ui.Button_close,
                          1,
                          (wx.wxALL+wx.wxALIGN_RIGHT+wx.wxALIGN_CENTER_VERTICAL),
                          5)

    ui.window:Connect(ID.BUTTON_CLOSE,
                      wx.wxEVT_COMMAND_BUTTON_CLICKED,
                      function() ui.window:Destroy() end)

    -- set sizer
    ui.window:SetSizer(ui.FlexGridSizer1)
    ui.FlexGridSizer1:Fit(ui.window)
    ui.FlexGridSizer1:SetSizeHints(ui.window)

    -- show window
    ui.window:ShowModal()
end

--------------------------------------------------------------------------------
-- @brief  Show license dialog
-- @param  parent       parent window
-- @return None
--------------------------------------------------------------------------------
local function ShowLicenseDialog(parent)
        local ui = {}
        local ID = {}
        ID.BUTTON_CLOSE = wx.wxNewId()

        -- create license dialog
        ui.window = wx.wxDialog(parent, wx.wxID_ANY, "License")
        ui.FlexGridSizer1 = wx.wxFlexGridSizer(0, 1, 0, 0)

        -- add textctrl with license
        ui.TextCtrl = wx.wxTextCtrl(ui.window,
                                    wx.wxID_ANY,
                                    "Configtool\nCopyright © 2017 Daniel Zorychta\n\n"..
                                    "This program is free software; you can "..
                                    "redistribute it and/or modify it under "..
                                    "the terms of the GNU General Public License "..
                                    "as published by the Free Software Foundation; "..
                                    "either version 2 of the licence, or (at your "..
                                    "option) any later version.\n\n"..
                                    "This program is distributed in the hope that "..
                                    "it will be useful, but WITHOUT ANY WARRANTY; "..
                                    "without even the implied warranty of "..
                                    "MERCHANTABILITY or FITNESS FOR A PARTICULAR "..
                                    "PURPOSE. See the GNU General Public License "..
                                    "for more details.\n\n"..
                                    "You should have received a copy of the GNU "..
                                    "General Public License along with this program. "..
                                    "You may also obtain a copy of the GNU General "..
                                    "Public License from the Free Software Foundation "..
                                    "by visiting their web site (http://www.fsf.org/) "..
                                    "or by writing to the Free Software Foundation, "..
                                    "Inc., 51 Franklin St, Fifth Floor, Boston, MA "..
                                    "02110-1301  USA\n",
                                    wx.wxDefaultPosition,
                                    wx.wxSize(400, 300),
                                    (wx.wxTE_MULTILINE+wx.wxTE_AUTO_URL))

        ui.FlexGridSizer1:Add(ui.TextCtrl,
                              1,
                              (wx.wxALL+wx.wxALIGN_CENTER_HORIZONTAL+wx.wxALIGN_CENTER_VERTICAL),
                              5)

        -- add close button
        ui.Button_close = wx.wxButton(ui.window, ID.BUTTON_CLOSE, "Close")

        ui.FlexGridSizer1:Add(ui.Button_close, 1,
                              (wx.wxALL+wx.wxALIGN_RIGHT+wx.wxALIGN_CENTER_VERTICAL), 5)

        ui.window:Connect(ID.BUTTON_CLOSE,
                          wx.wxEVT_COMMAND_BUTTON_CLICKED,
                          function() ui.window:Destroy() end)

        -- set sizer
        ui.window:SetSizer(ui.FlexGridSizer1)
        ui.FlexGridSizer1:Fit(ui.window)
        ui.FlexGridSizer1:SetSizeHints(ui.window)

        -- show window
        ui.window:ShowModal()
end

--------------------------------------------------------------------------------
-- @brief  Function creates a new window
-- @param  parent       parent window
-- @return New window handle
--------------------------------------------------------------------------------
function ShowAboutWindow(parent)
    local font

    ID = {}
    ID.HYPERLINKCTRL_WEBSITE = wx.wxNewId()
    ID.BUTTON_CREDITS        = wx.wxNewId()
    ID.BUTTON_LICENSE        = wx.wxNewId()
    ID.BUTTON_CLOSE          = wx.wxNewId()

    -- create dialog
    ui.window = wx.wxDialog(parent, wx.wxID_ANY, "About")
    ui.FlexGridSizer1 = wx.wxFlexGridSizer(0, 1, 0, 0)

    -- add program name string
    ui.StaticText = wx.wxStaticText(ui.window, wx.wxID_ANY, "Configtool "..VERSION)
    font = ui.StaticText:GetFont()
    font:SetPointSize(font:GetPointSize() + 8)
    font:SetWeight(wx.wxFONTWEIGHT_BOLD)
    ui.StaticText:SetFont(font)
    ui.FlexGridSizer1:Add(ui.StaticText, 1,
        (wx.wxALL+wx.wxALIGN_CENTER_HORIZONTAL+wx.wxALIGN_CENTER_VERTICAL), 5)

    -- add short program description
    ui.StaticText = wx.wxStaticText(ui.window, wx.wxID_ANY,
        "Configtool is a graphical tool that support configuration of C/C++ flags.")

    ui.FlexGridSizer1:Add(ui.StaticText, 1,
        (wx.wxALL+wx.wxALIGN_CENTER_HORIZONTAL+wx.wxALIGN_CENTER_VERTICAL), 5)

    -- add copyright notice
    ui.StaticText = wx.wxStaticText(ui.window, wx.wxID_ANY,
                                    "Copyright © 2017 Daniel Zorychta")
    font = ui.StaticText:GetFont()
    font:SetPointSize(font:GetPointSize() - 1)
    ui.StaticText:SetFont(font)
    ui.FlexGridSizer1:Add(ui.StaticText, 1,
        (wx.wxALL+wx.wxALIGN_CENTER_HORIZONTAL+wx.wxALIGN_CENTER_VERTICAL), 5)

    -- add project website link
    ui.HyperlinkCtrl_website = wx.wxHyperlinkCtrl(ui.window,
        ID.HYPERLINKCTRL_WEBSITE, URL, URL,
        wx.wxDefaultPosition, wx.wxDefaultSize,
        (wx.wxHL_CONTEXTMENU+wx.wxHL_ALIGN_CENTRE+wx.wxNO_BORDER))

    ui.FlexGridSizer1:Add(ui.HyperlinkCtrl_website, 1,
        (wx.wxALL+wx.wxALIGN_CENTER_HORIZONTAL+wx.wxALIGN_CENTER_VERTICAL), 5)

    -- add sizer that contains buttons
    ui.BoxSizer1 = wx.wxBoxSizer(wx.wxHORIZONTAL)

    -- add credits button
    ui.Button_credits = wx.wxButton(ui.window, ID.BUTTON_CREDITS, "Credits")

    ui.BoxSizer1:Add(ui.Button_credits, 1,
        (wx.wxALL+wx.wxALIGN_CENTER_HORIZONTAL+wx.wxALIGN_CENTER_VERTICAL), 5)

    ui.window:Connect(ID.BUTTON_CREDITS,
                      wx.wxEVT_COMMAND_BUTTON_CLICKED,
                      function() ShowCreditsDialog(ui.window) end)

    -- add license button
    ui.Button_license = wx.wxButton(ui.window, ID.BUTTON_LICENSE, "License")

    ui.BoxSizer1:Add(ui.Button_license, 1,
        (wx.wxALL+wx.wxALIGN_CENTER_HORIZONTAL+wx.wxALIGN_CENTER_VERTICAL), 5)

    ui.window:Connect(ID.BUTTON_LICENSE,
                      wx.wxEVT_COMMAND_BUTTON_CLICKED,
                      function() ShowLicenseDialog(ui.window) end)

    -- add separator
    ui.BoxSizer1:Add(0, 0, 1,
        (wx.wxALL+wx.wxALIGN_CENTER_HORIZONTAL+wx.wxALIGN_CENTER_VERTICAL), 5)

    -- add close button
    ui.Button_close = wx.wxButton(ui.window, ID.BUTTON_CLOSE, "Close")

    ui.BoxSizer1:Add(ui.Button_close, 1,
        (wx.wxALL+wx.wxALIGN_CENTER_HORIZONTAL+wx.wxALIGN_CENTER_VERTICAL), 5)

    ui.window:Connect(ID.BUTTON_CLOSE,
                      wx.wxEVT_COMMAND_BUTTON_CLICKED,
                      function() ui.window:Destroy() end)

    -- add button sizer to main sizer
    ui.FlexGridSizer1:Add(ui.BoxSizer1, 1,
        (wx.wxALL+wx.wxEXPAND+wx.wxALIGN_CENTER_HORIZONTAL+wx.wxALIGN_CENTER_VERTICAL), 5)

    -- fit window
    ui.FlexGridSizer1:Fit(ui.window)
    ui.FlexGridSizer1:SetSizeHints(ui.window)

    -- set main sizer
    ui.window:SetSizer(ui.FlexGridSizer1)

    -- show window
    ui.window:ShowModal()
end
