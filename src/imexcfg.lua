--[[============================================================================
@file    imexcfg.lua

@author  Daniel Zorychta

@brief   The IMport EXport ConFig functionallity

@note    Copyright (C) 2015 Daniel Zorychta <daniel.zorychta@gmail.com>

         This program is free software; you can redistribute it and/or modify
         it under the terms of the GNU General Public License as published by
         the  Free Software  Foundation;  either version 2 of the License, or
         any later version.

         This  program  is  distributed  in the hope that  it will be useful,
         but  WITHOUT  ANY  WARRANTY;  without  even  the implied warranty of
         MERCHANTABILITY  or  FITNESS  FOR  A  PARTICULAR  PURPOSE.  See  the
         GNU General Public License for more details.

         You  should  have received a copy  of the GNU General Public License
         along  with  this  program;  if not,  write  to  the  Free  Software
         Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.


==============================================================================]]

require("utils")

--==============================================================================
-- GLOBAL OBJECTS
--==============================================================================

--==============================================================================
-- LOCAL OBJECTS
--==============================================================================
local CFG_FILE_ID       = "cb9d12372186c874364d4e8b77d794a3"
local CFG_FILE_VERSION  = "5"

--==============================================================================
-- LOCAL FUNCTIONS
--==============================================================================
--------------------------------------------------------------------------------
-- @brief  Function try found specified key in the selected line
-- @param  line         line where key is finding
-- @return flag name, value
--------------------------------------------------------------------------------
local function GetLineFlag(line)
    if line:match('^%s*#%s*define%s+__.*__%s+.*$') then
        return line:match('^%s*#%s*define%s+(__.*__)%s+(.*)$')
    elseif line:match('^%s*__.*__%s*=%s*.*$') then
        return line:match('^%s*(__.*__)%s*=%s*(.*)$')
    else
        return nil, nil
    end
end

--------------------------------------------------------------------------------
-- @brief  Load table from file
-- @param  file         file to read from
-- @param  Pulse        function for update progress dialog
-- @return On success table is returned, otherwise nil
--------------------------------------------------------------------------------
local function CollectSettings(tab, path, Pulse)
    local count, files = wx.wxDir().GetAllFiles(GetConfigPath(), "*.h")
    for i, name in ipairs(files) do
        name = wx.wxDos2UnixFilename(name)

        local fname = name:gsub(GetConfigPath(), "")

        if ui.Frame ~= nil then
            Pulse("Exporting: "..fname)
            ui.Frame:SetStatusText("Exporting: "..fname, 1)
        end

        tab[fname] = {}

        local flagOld = ""
        local valOld  = ""
        local file    = io.open(name, "rb")

        for line in file:lines() do
            local flag, val = GetLineFlag(line)

            if flag ~= nil and val ~= nil then
                if flag ~= flagOld then
                    local pair = {key = flag, value = val}
                    table.insert(tab[fname], pair)
                    flagOld = flag
                    valOld  = val
                elseif val ~= valOld then
                    print("CollectSettings(): duplicated flag: '"..flag..
                            "' with different values: '"..valOld.."', '"..val.."'")
                end
            end
        end

        file:close()

        if #tab[fname] == 0 then
            tab[fname] = nil
        end
    end
end

--------------------------------------------------------------------------------
-- @brief  Apply configuration to selected file
-- @param  setup        configuration table
-- @param  Pulse        function for update progress dialog
-- @return true on success, otherwise false
--------------------------------------------------------------------------------
local function ApplySettings(setup, Pulse)
    errors = ""

    for file, keys in pairs(setup.file) do
        if ui.Frame ~= nil then
            ui.Frame:SetStatusText("Importing: "..file, 1)
            Pulse("Importing: "..file)
        end

        if KeyWrite(file, keys) == false then
            print("You have not permissions to write project configuration or file not exists.")
            errors = errors.." - file not exists: "..file.."\n"
        end
    end

    if errors ~= "" then
        if ui.Frame ~= nil then
            ShowInfoMessage("Configuration import",
                            "Configuration can be incomplete, some errors occurred:\n"..errors,
                            ui.Frame)
        end
    end

    return true
end

--==============================================================================
-- PUBLIC FUNCTIONS
--==============================================================================
--------------------------------------------------------------------------------
-- @brief  Save configuration to selected file
-- @param  path         file to export
-- @return None
--------------------------------------------------------------------------------
function ExportConfiguration(path, parser)
    local setup   = {}
    setup.file    = {}
    setup.ID      = CFG_FILE_ID
    setup.version = CFG_FILE_VERSION

    if ui.Frame ~= nil then
        ui.Frame:SetStatusText("Collecting files to export...", 1)
        local steps = wx.wxDir().GetAllFiles(GetConfigPath(), "*.h")

        local progress = wx.wxProgressDialog("Configuration export",
                                            "",
                                            steps + 1,
                                            ui.Frame,
                                            (wx.wxPD_APP_MODAL + wx.wxPD_AUTO_HIDE))

        progress:SetMinSize(wx.wxSize(450, 150))
        progress:SetSize(wx.wxSize(450, 150))
        progress:Centre()

        progress.value = 0
        progress.Pulse = function(self, msg)
                            self:Update(self.value, msg)
                            self.value = self.value + 1
                        end

        progress:Pulse("Calling user event...")
        if parser ~= nil then parser:CallEvent("ConfigExport") end

        CollectSettings(setup.file, GetConfigPath(), function(msg) progress:Pulse(msg) end)

        progress:Pulse("Saving collected setting to file...")
        SaveTable(setup, path)

        progress:Destroy()
    else
        CollectSettings(setup.file, GetConfigPath())
        SaveTable(setup, path)
    end
end

--------------------------------------------------------------------------------
-- @brief  Save configuration to selected file
-- @param  path         file to export
-- @return None
--------------------------------------------------------------------------------
function ImportConfiguration(path, parser)
    local setup = LoadTable(path)

    if setup == nil or setup.ID ~= CFG_FILE_ID or setup.version ~= CFG_FILE_VERSION then
        if ui.Frame ~= nil then
            ShowInfoMessage("Configuration import", "Incorrect configuration", ui.Frame)
        else
            print("Incorrect configuration")
            os.exit(1)
        end
    else
        local progress

        if ui.Frame ~= nil then
            ui.Frame:SetStatusText("Calculating number of files to import...", 1)
            local steps = 1 for _, keys in pairs(setup.file) do steps = steps + 1 end

            progress = wx.wxProgressDialog("Configuration import",
                                           "",
                                           steps,
                                           ui.Frame,
                                           (wx.wxPD_APP_MODAL + wx.wxPD_AUTO_HIDE))

            progress:SetMinSize(wx.wxSize(450, 150))
            progress:SetSize(wx.wxSize(450, 150))
            progress:Centre()

            progress.value = 1
            progress.Pulse = function(self, msg)
                                self:Update(self.value, msg)
                                self.value = self.value + 1
                            end

            progress:Pulse("Calling user event...")
        end

        if parser ~= nil then parser:CallEvent("ConfigImport") end

        if ui.Frame ~= nil then
            ApplySettings(setup, function(msg) progress:Pulse(msg) end)
            progress:Destroy()
        else
            ApplySettings(setup)
        end
    end
end
