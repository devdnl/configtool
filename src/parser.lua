--[[============================================================================
@file    parser.lua

@author  Daniel Zorychta

@brief   Parser of configuration headers

@note    Copyright (C) 2015 Daniel Zorychta <daniel.zorychta@gmail.com>

         This program is free software; you can redistribute it and/or modify
         it under the terms of the GNU General Public License as published by
         the  Free Software  Foundation;  either version 2 of the License, or
         any later version.

         This  program  is  distributed  in the hope that  it will be useful,
         but  WITHOUT  ANY  WARRANTY;  without  even  the implied warranty of
         MERCHANTABILITY  or  FITNESS  FOR  A  PARTICULAR  PURPOSE.  See  the
         GNU General Public License for more details.

         You  should  have received a copy  of the GNU General Public License
         along  with  this  program;  if not,  write  to  the  Free  Software
         Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.


==============================================================================]]

require("wx")
require("utils")

--============================================================================--
-- PRIVATE FUNCTIONS
--============================================================================--
--------------------------------------------------------------------------------
-- @brief  Function create error message
-- @param  msg              message to print
-- @return None
--------------------------------------------------------------------------------
local function GetErrorMsg(self, msg)
    return "[Error] "..self._FileName..":"..self._ChunkBegin.."-"..self._ChunkEnd..": "..msg
end

--------------------------------------------------------------------------------
-- @brief  Function create warning message
-- @param  msg              message to print
-- @return None
--------------------------------------------------------------------------------
local function GetWarningMsg(self, msg)
    return "[Warning] "..self._FileName..":"..self._ChunkBegin.."-"..self._ChunkEnd..": "..msg
end

--------------------------------------------------------------------------------
-- @brief  Function prints error and close application
-- @param  msg              message to print
-- @return None
--------------------------------------------------------------------------------
local function PrintError(self, msg)
    print(GetErrorMsg(self, msg))
    ShowErrorMessage("Parsing error", GetErrorMsg(self, msg), ui.Frame, false)
    self._script.finish = true
end

--------------------------------------------------------------------------------
-- @brief  Function prints warning
-- @param  msg              message to print
-- @return None
--------------------------------------------------------------------------------
local function PrintWarning(self, msg)
    print(GetWarningMsg(self, msg))
    ShowInfoMessage("Parsing warning", GetWarningMsg(self, msg), ui.Frame)
end

--------------------------------------------------------------------------------
-- @brief  Parser assert
-- @param  msg              message to print
-- @return None
--------------------------------------------------------------------------------
local function ParserAssert(self, test, msg)
    if test == false then
        print(GetErrorMsg(self, msg))
        os.exit()
    end
end

--------------------------------------------------------------------------------
-- @brief  Call selected event (parser event)
-- @param  event
-- @return None
--------------------------------------------------------------------------------
local function CallEvent(self, event)
    if self._Event[event] then
        return self._Event[event]()
    end
end

--------------------------------------------------------------------------------
-- @brief  Call loaded script
-- @param  self
-- @param  script   script to run
-- @return None
--------------------------------------------------------------------------------
local function ExecuteScript(self, script)
    local status, err = pcall(loadstring("local this = _SCRIPT_SELF "..script))
    if status == false then
        PrintError(self, err:gsub("%[.*%]", "Line"))
    end

    return ""
end

--------------------------------------------------------------------------------
-- @brief  Function parse selected file
-- @param  self         class object
-- @return None
--------------------------------------------------------------------------------
local function RunParser(self)
    _SCRIPT_SELF = self._script

    local file = io.open(GetConfigPath()..self._FileName, "rb")
    if file == nil then
        PrintError(self, "File does not exist: "..self._FileName)
    end

    local scriptArea = false
    local script     = ""
    local lineNo     = 1

    for line in file:lines() do
        if line:match("^%s*#%s*define%s+"..self._KeyRegExp) then
            self._CurrentFlag, value = line:match("^%s*#%s*define%s+("..self._KeyRegExp..")%s+(.*)")

            if self._CurrentFlag ~= nil or value ~= nil then

                self._Flag[self._CurrentFlag] = {}
                self._Flag[self._CurrentFlag].Value = value
                self._Flag[self._CurrentFlag].OrginalValue = value
                self._Flag[self._CurrentFlag].GetValue = function() return self._Flag[self._CurrentFlag].Value end
                self._Flag[self._CurrentFlag].SetValue = function(value) self._Flag[self._CurrentFlag].Value = value end

                script = ExecuteScript(self, script)
            end

        elseif line:match("^/%*%-%-") or line:match("^#/%*%-%-") then
            script           = ""
            scriptArea       = true
            self._ChunkBegin = lineNo

        elseif line:match("^%-%-%*/") or line:match("^#%-%-%*/") then
            scriptArea     = false
            self._ChunkEnd = lineNo

        elseif line:match("^%+%+%*/") or line:match("^#%+%+%*/") then
            scriptArea     = false
            self._ChunkEnd = lineNo

            script = ExecuteScript(self, script)

        elseif scriptArea == true then
            if line:match("^(#)") then
                script = script..line:match("^#(.*)").."\n"
            else
                script = script..line.."\n"
            end
        end

        lineNo = lineNo + 1

        if self._script.finish == true then break end
    end

    self._script.finish = false

    file:close()

    _SCRIPT_SELF = nil
end

--------------------------------------------------------------------------------
-- @brief  Adds widget to panel (by using sizer)
-- @param  self             class object
-- @param  widget           widget to add
-- @param  flags            extra flags
-- @return None
--------------------------------------------------------------------------------
local function AddToWindow(self, widget, flags)
    local defaultFlags = (wx.wxALL + wx.wxALIGN_LEFT + wx.wxALIGN_CENTER_VERTICAL)

    if type(flags) == "number" then defaultFlags = defaultFlags + flags end

    self._wx.ObjectSizer:Add(widget, 0, defaultFlags, 3)
end

--------------------------------------------------------------------------------
-- @brief  Create Combobox widget
-- @param  self         class object
-- @param  FLAG         flag object
-- @param  text         widget text
-- @return None
--------------------------------------------------------------------------------
local function GenericAddCombobox(self, FLAG, text)
    if text ~= nil then
        ParserAssert(self, type(text) == "string", "Combobox::text: string expected (got "..type(text)..")")
        FLAG.wx.Label = wx.wxStaticText(self._wx.Window, wx.wxID_ANY, text)
        AddToWindow(self, FLAG.wx.Label)
    end

    FLAG.wx.ChoiceId = wx.wxNewId()
    FLAG.wx.Choice   = wx.wxChoice(self._wx.Window, FLAG.wx.ChoiceId)
    AddToWindow(self, FLAG.wx.Choice, wx.wxEXPAND)

    self._wx.Window:Connect(FLAG.wx.ChoiceId, wx.wxEVT_COMMAND_CHOICE_SELECTED,
        function()
            FLAG.Value = FLAG.Item:GetValueByIdx(FLAG.wx.Choice:GetSelection() + 1)
            if FLAG.wx.Event["clicked"] then FLAG.wx.Event["clicked"]() end
        end
    )

    FLAG.AddItem = function(itemName, itemValue)
        FLAG.Item:AddItem(itemName, itemValue)
        FLAG.wx.Choice:Append(itemName)
        FLAG.wx.Choice:SetSelection(FLAG.Item:GetIdxByValue(FLAG.Value) - 1)
    end

    FLAG.SetValue = function(Value)
        FLAG.Value = Value
        FLAG.wx.Choice:SetSelection(FLAG.Item:GetIdxByValue(Value) - 1)
    end

    FLAG.Enable = function(en)
        FLAG.wx.Choice:Enable(en)
    end

    FLAG.SetToolTip = function(tip)
        FLAG.wx.Choice:SetToolTip(tip)
        if FLAG.wx.Label ~= nil then FLAG.wx.Label:SetToolTip(tip) end
    end
end

--------------------------------------------------------------------------------
-- @brief  Create Textbox widget
-- @param  self         class object
-- @param  FLAG         flag object
-- @param  text         widget text
-- @return None
--------------------------------------------------------------------------------
local function GenericAddTextbox(self, FLAG, text)
    if text ~= nil then
        ParserAssert(self, type(text) == "string", "Textbox::text: string expected (got "..type(text)..")")
        FLAG.wx.Label = wx.wxStaticText(self._wx.Window, wx.wxID_ANY, text)
        AddToWindow(self, FLAG.wx.Label)
    end

    FLAG.wx.TextboxId = wx.wxNewId()
    FLAG.wx.Textbox   = wx.wxComboBox(self._wx.Window, FLAG.wx.TextboxId, FLAG.Value)
    AddToWindow(self, FLAG.wx.Textbox, wx.wxEXPAND)

    self._wx.Window:Connect(FLAG.wx.TextboxId, wx.wxEVT_COMMAND_TEXT_UPDATED,
        function()
            FLAG.Value = FLAG.wx.Textbox:GetValue()
        end
    )

    self._wx.Window:Connect(FLAG.wx.TextboxId, wx.wxEVT_COMMAND_COMBOBOX_SELECTED,
        function()
            FLAG.Value = FLAG.wx.Textbox:GetValue()
            if FLAG.wx.Event["clicked"] then FLAG.wx.Event["clicked"]() end
        end
    )

    FLAG.AddItem = function(itemName, itemValue)
        FLAG.wx.Textbox:Append(itemName)
    end

    FLAG.SetValue = function(Value)
        FLAG.Value = Value
        FLAG.wx.Textbox:SetValue(Value)
    end

    FLAG.Enable = function(en)
        FLAG.wx.Textbox:Enable(en)
    end

    FLAG.SetToolTip = function(tip)
        FLAG.wx.Textbox:SetToolTip(tip)
        if FLAG.wx.Label ~= nil then FLAG.wx.Label:SetToolTip(tip) end
    end
end

--------------------------------------------------------------------------------
-- @brief  Create Label widget
-- @param  self         class object
-- @param  FLAG         flag object
-- @param  text         widget text
-- @param  fontSize     font size (in pt)
-- @param  fontStyle    font style ("bold", "italic", "bold+italic", "italic+bold")
-- @param  color        font color ("red", "green", "blue", "black")
-- @return None
--------------------------------------------------------------------------------
local function GenericAddLabel(self, FLAG, text, fontSize, fontStyle, color)
    ParserAssert(self, type(text) == "string", "Label::text: string expected (got "..type(text)..")")

    FLAG.Value      = text
    FLAG.wx.LabelId = wx.wxNewId()
    FLAG.wx.Label   = wx.wxStaticText(self._wx.Window, FLAG.wx.LabelId, text)

    if type(fontSize) == "number" then
        local style  = wx.wxFONTSTYLE_NORMAL
        local weight = wx.wxNORMAL

        if fontStyle == "italic" then
            style = wx.wxFONTSTYLE_ITALIC
        elseif fontStyle == "bold" then
            weight = wx.wxBOLD
        elseif fontStyle == "italic+bold" or fontStyle == "bold+italic" then
            style  = wx.wxFONTSTYLE_ITALIC
            weight = wx.wxBOLD
        end

        FLAG.wx.Label:SetFont(wx.wxFont(fontSize, wx.wxDEFAULT, style, weight))

        if type(color) == "string" then
            if color == "red" then
                FLAG.wx.Label:SetForegroundColour(wx.wxColour(255,0,0))
            elseif color == "green" then
                FLAG.wx.Label:SetForegroundColour(wx.wxColour(0,255,0))
            elseif color == "blue" then
                FLAG.wx.Label:SetForegroundColour(wx.wxColour(0,0,255))
            elseif color == "black" then
                FLAG.wx.Label:SetForegroundColour(wx.wxColour(0,0,0))
            end
        end
    end

    AddToWindow(self, FLAG.wx.Label)

    FLAG.SetValue = function(value)
        FLAG.Value = value
        FLAG.wx.Label:SetLabel(value)
    end

    FLAG.SetToolTip = function(tip)
        FLAG.wx.Label:SetToolTip(tip)
    end
end

--------------------------------------------------------------------------------
-- @brief  Create BitmapButton widget
-- @param  self         class object
-- @param  FLAG         flag object
-- @param  bitmapPath   bitmap path
-- @return None
--------------------------------------------------------------------------------
local function GenericAddBitmapButton(self, FLAG, bitmapPath)
    ParserAssert(self, type(bitmapPath) == "string", "BitmapButton::bitmapPath: string expected (got "..type(bitmapPath)..")")

    FLAG.wx.ButtonId = wx.wxNewId()
    FLAG.wx.Button   = wx.wxBitmapButton(self._wx.Window, FLAG.wx.ButtonId, wx.wxBitmap(bitmapPath))
    AddToWindow(self, FLAG.wx.Button)

    self._wx.Window:Connect(FLAG.wx.ButtonId, wx.wxEVT_COMMAND_BUTTON_CLICKED,
        function()
            if FLAG.wx.Event["clicked"] then FLAG.wx.Event["clicked"]() end
        end
    )

    FLAG.Enable = function(en)
        FLAG.wx.Button:Enable(en)
    end

    FLAG.SetToolTip = function(tip)
        FLAG.wx.Button:SetToolTip(tip)
    end
end

--------------------------------------------------------------------------------
-- @brief  Create Value widget
-- @param  self         class object
-- @param  FLAG         flag object
-- @return None
--------------------------------------------------------------------------------
local function GenericAddValue(self, FLAG)
    -- all basic methodes defined earlier
end

--------------------------------------------------------------------------------
-- @brief  Create Void widget
-- @param  self         class object
-- @param  FLAG         flag object
-- @return None
--------------------------------------------------------------------------------
local function GenericAddVoid(self, FLAG)
    FLAG.wx.Label = wx.wxStaticText(self._wx.Window, wx.wxID_ANY, "")
    AddToWindow(self, FLAG.wx.Label)
end

--------------------------------------------------------------------------------
-- @brief  Create Checkbox widget
-- @param  self         class object
-- @param  FLAG         flag object
-- @param  text         widget text
-- @param  fontSize     font size (in pt)
-- @param  fontStyle    font style ("bold", "italic", "bold+italic", "italic+bold")
-- @return None
--------------------------------------------------------------------------------
local function GenericAddCheckbox(self, FLAG, text)
    ParserAssert(self, type(text) == "string", "Checkbox::text: string expected (got "..type(text)..")")

    FLAG.wx.CheckboxId = wx.wxNewId()
    FLAG.wx.Checkbox   = wx.wxCheckBox(self._wx.Window, FLAG.wx.CheckboxId, text)
    AddToWindow(self, FLAG.wx.Checkbox)

    self._wx.Window:Connect(FLAG.wx.CheckboxId, wx.wxEVT_COMMAND_CHECKBOX_CLICKED,
        function()
            FLAG.Value = iff(FLAG.wx.Checkbox:IsChecked(), "_YES_", "_NO_")
            if FLAG.wx.Event["clicked"] then FLAG.wx.Event["clicked"]() end
        end
    )

    FLAG.SetValue = function(value)
        FLAG.Value = value
        if value == "1" or value == 1 or value == "_YES_" then
            FLAG.wx.Checkbox:SetValue(true)
        else
            FLAG.wx.Checkbox:SetValue(false)
        end
    end

    FLAG.Enable = function(en)
        FLAG.wx.Checkbox:Enable(en)
    end

    FLAG.SetToolTip = function(tip)
        FLAG.wx.Checkbox:SetToolTip(tip)
    end

    FLAG.SetValue(FLAG.Value)
end

--------------------------------------------------------------------------------
-- @brief  Create Hyperlink widget
-- @param  self         class object
-- @param  FLAG         flag object
-- @param  text         widget text
-- @param  fontSize     font size (in pt)
-- @param  fontStyle    font style ("bold", "italic", "bold+italic", "italic+bold")
-- @return None
--------------------------------------------------------------------------------
local function GenericAddHyperlink(self, FLAG, text, link)
    ParserAssert(self, type(text) == "string", "Hyperlink::text: string expected (got "..type(text)..")")

    FLAG.wx.HyperlinkId = wx.wxNewId()
    FLAG.wx.Hyperlink   = wx.wxHyperlinkCtrl(self._wx.Window, FLAG.wx.HyperlinkId,
                                            text, "", wx.wxDefaultPosition, wx.wxDefaultSize, wx.wxBORDER_NONE)

    AddToWindow(self, FLAG.wx.Hyperlink)

    self._wx.Window:Connect(FLAG.wx.HyperlinkId, wx.wxEVT_COMMAND_HYPERLINK,
        function()
            if FLAG.wx.Event["clicked"] then
                FLAG.wx.Event["clicked"]()
            else
                wx.wxLaunchDefaultBrowser(iff(type(link) == "string", link, "<link error>"))
            end
        end
    )

    FLAG.Enable = function(en)
        FLAG.wx.Hyperlink:Enable(en)
    end

    FLAG.SetToolTip = function(tip)
        FLAG.wx.Hyperlink:SetToolTip(tip)
    end
end

--------------------------------------------------------------------------------
-- @brief  Create Editline widget
-- @param  self         class object
-- @param  FLAG         flag object
-- @param  quotation    value in quotation
-- @param  text         widget text
-- @return None
--------------------------------------------------------------------------------
local function GenericAddEditline(self, FLAG, quotation, text)
    ParserAssert(self, type(quotation) == "boolean", "Editline::quotation: boolean expected (got "..type(quotation)..")")

    if text ~= nil then
        ParserAssert(self, type(text) == "string", "Editline::text: string expected (got "..type(text)..")")

        FLAG.wx.Label = wx.wxStaticText(self._wx.Window, wx.wxID_ANY, text)
        AddToWindow(self, FLAG.wx.Label)
    end

    FLAG.wx.Quotation  = quotation
    FLAG.wx.EditlineId = wx.wxNewId()
    FLAG.wx.Editline   = wx.wxTextCtrl(self._wx.Window,
                                       FLAG.wx.EditlineId,
                                       iff(FLAG.wx.Quotation == true,
                                           FLAG.Value:gsub('^"', ""):gsub('"$', ""),
                                           FLAG.Value),
                                       wx.wxDefaultPosition,
                                       wx.wxDefaultSize)

    AddToWindow(self, FLAG.wx.Editline, wx.wxEXPAND)

    self._wx.Window:Connect(FLAG.wx.EditlineId, wx.wxEVT_COMMAND_TEXT_UPDATED,
        function()
            if FLAG.wx.Quotation then
                FLAG.Value = '"'..FLAG.wx.Editline:GetValue()..'"'
            else
                FLAG.Value = FLAG.wx.Editline:GetValue()
            end
        end
    )

    FLAG.SetValue = function(value)
        FLAG.Value = value
        FLAG.wx.Editline:SetValue(iff(FLAG.wx.Quotation == true,
                                      FLAG.Value:gsub('^"', ""):gsub('"$', ""),
                                      FLAG.Value))
    end

    FLAG.Enable = function(en)
        FLAG.wx.Editline:Enable(en)
    end

    FLAG.SetToolTip = function(tip)
        FLAG.wx.Editline:SetToolTip(tip)
        if FLAG.wx.Label ~= nil then FLAG.wx.Label:SetToolTip(tip) end
    end
end

--------------------------------------------------------------------------------
-- @brief  Create Editline widget
-- @param  self         class object
-- @param  FLAG         flag object
-- @param  minValue     minimal value
-- @param  maxValue     maximal value
-- @param  text         widget text
-- @return None
--------------------------------------------------------------------------------
local function GenericAddSpinbox(self, FLAG, minValue, maxValue, text)
    ParserAssert(self, type(minValue) == "number", "Spinbox::minValue: number expected (got "..type(minValue)..")")
    ParserAssert(self, type(maxValue) == "number", "Spinbox::maxValue: number expected (got "..type(maxValue)..")")

    if text ~= nil then
        ParserAssert(self, type(text) == "string", "Spinbox::text: string expected (got "..type(text)..")")

        FLAG.wx.Label = wx.wxStaticText(self._wx.Window, wx.wxID_ANY, text)
        AddToWindow(self, FLAG.wx.Label)
    end

    FLAG.wx.SpinboxId = wx.wxNewId()
    FLAG.wx.Spinbox   = wx.wxSpinCtrl(self._wx.Window,
                                      FLAG.wx.SpinboxId,
                                      FLAG.Value,
                                      wx.wxDefaultPosition,
                                      wx.wxDefaultSize,
                                      0, minValue, maxValue)

    AddToWindow(self, FLAG.wx.Spinbox, wx.wxEXPAND)

    local Event = function()
            if FLAG.wx.Quotation then
                FLAG.Value = '"'..FLAG.wx.Editline:GetValue()..'"'
            else
                FLAG.Value = FLAG.wx.Editline:GetValue()
            end
    end

    self._wx.Window:Connect(FLAG.wx.SpinboxId, wx.wxEVT_COMMAND_SPINCTRL_UPDATED,
        function()
            FLAG.Value = tostring(FLAG.wx.Spinbox:GetValue())
            if FLAG.wx.Event["clicked"] then FLAG.wx.Event["clicked"]() end
        end
    )

    self._wx.Window:Connect(FLAG.wx.SpinboxId, wx.wxEVT_COMMAND_TEXT_UPDATED,
        function()
            FLAG.Value = tostring(FLAG.wx.Spinbox:GetValue())
            if FLAG.wx.Event["clicked"] then FLAG.wx.Event["clicked"]() end
        end
    )

    FLAG.SetValue = function(value)
        FLAG.Value = value
        FLAG.wx.Spinbox:SetValue(tonumber(value))
    end

    FLAG.Enable = function(en)
        FLAG.wx.Spinbox:Enable(en)
    end

    FLAG.SetToolTip = function(tip)
        FLAG.wx.Spinbox:SetToolTip(tip)
        if FLAG.wx.Label ~= nil then FLAG.wx.Label:SetToolTip(tip) end
    end
end

--------------------------------------------------------------------------------
-- @brief  Function creates widgets
-- @param  widgetType       Widget Type: Combobox, Label
-- @param  widgetName       Widget Name (or flag name)
-- @param  widgetArg1       Argument 1 (optional)
-- @param  widgetArg2       Argument 2 (optional)
-- @param  widgetArg3       Argument 3 (optional)
-- @param  widgetArg4       Argument 4 (optional)
-- @return None
--------------------------------------------------------------------------------
local function GenericAddWidget(self,
                                widgetType, widgetName,
                                Arg1, Arg2, Arg3, Arg4)

    -- widget shortcut
    local FLAG = self._Flag[widgetName]

    -- create flag default structure
    FLAG.wx         = {}
    FLAG.wx.Type    = widgetType
    FLAG.wx.Event   = {}
    FLAG.Item       = NewItemTable()
    FLAG.AddItem    = function(itemName, itemValue) FLAG.Item:AddItem(itemName, itemValue) end
    FLAG.GetValue   = function() return FLAG.Value end
    FLAG.SetValue   = function(value) FLAG.Value = value end
    FLAG.SetEvent   = function(event, func) FLAG.wx.Event[event] = func end
    FLAG.Enable     = function(en) end
    FLAG.SetToolTip = function() end

    -- widget creator lookup table
    local ADD_WIDGET = {}
    ADD_WIDGET["Combobox"]     = GenericAddCombobox
    ADD_WIDGET["Label"]        = GenericAddLabel
    ADD_WIDGET["Value"]        = GenericAddValue
    ADD_WIDGET["Void"]         = GenericAddVoid
    ADD_WIDGET["Checkbox"]     = GenericAddCheckbox
    ADD_WIDGET["Hyperlink"]    = GenericAddHyperlink
    ADD_WIDGET["Editline"]     = GenericAddEditline
    ADD_WIDGET["Spinbox"]      = GenericAddSpinbox
    ADD_WIDGET["Textbox"]      = GenericAddTextbox
    ADD_WIDGET["BitmapButton"] = GenericAddBitmapButton

    if ADD_WIDGET[widgetType] then
        ADD_WIDGET[widgetType](self, FLAG, Arg1, Arg2, Arg3, Arg4)
    else
        PrintError(self, "Unknown widget: "..widgetType.."\n"..
                         "Available widgets:\n"..
                         "  Combobox, {widget name}, [text]\n"..
                         "  Textbox {widget name}, [text]\n"..
                         "  Label, {widget name}, text, [fontSize, fontStyle, color]\n"..
                         "  Value, {widget name} (no GUI widget)\n"..
                         "  Void, {widget name} (invisible widget)\n"..
                         "  Checkbox, {widget name}, text\n"..
                         "  Hyperlink {widget name}, text, [link]\n"..
                         "  Editline {widget name}, quotation, [text]\n"..
                         "  Spinbox {widget name}, minValue, maxValue, [text]\n"..
                         "  BitmapButton {widget name}, bitmapPath\n"..
                         "\n"..
                         "Argument in {} defines name of widget and is accessible "..
                         "only by using AddExtraWidget() function.\n"..
                         "Argument in [] is optional."
        )
    end
end

--============================================================================--
-- FUNCTIONS ACCESSABLE FROM CONFIGURATION FILES
--============================================================================--
--------------------------------------------------------------------------------
-- @brief  Function creates widget in selected flag (current flag)
-- @param  widgetType       widget type
-- @param  Argx             widget arguments
-- @return None
--------------------------------------------------------------------------------
local function AddWidget(this, widgetType, Arg1, Arg2, Arg3, Arg4)
    local self = this.self

    ParserAssert(self, type(widgetType) == "string", "AddWidget(widgetType[, ...]): widgetType: string expected (got "..type(widgetType)..")")

    if self._wx.WindowSizer == nil then
        PrintError(self, "Layout not defined. Please define layout by using SetLayout() function.")
        error("Script parser stopped.")
    end

    if self._Flag[self._CurrentFlag].wx == nil then
        GenericAddWidget(self, widgetType, self._CurrentFlag, Arg1, Arg2, Arg3, Arg4)
    else
        PrintError(self, "Flag can be represented only by one widget!")
    end
end

--------------------------------------------------------------------------------
-- @brief  Function adds extra widget with selected name
-- @param  widgetType       widget type
-- @param  widgetName       widget name
-- @param  arg1             argument 1 (optional)
-- @param  arg2             argument 2 (optional)
-- @param  arg3             argument 3 (optional)
-- @param  arg4             argument 4 (optional)
-- @return None
--------------------------------------------------------------------------------
local function AddExtraWidget(this, widgetType, widgetName, arg1, arg2, arg3, arg4)
    local self = this.self

    ParserAssert(self, type(widgetType) == "string", "AddExtraWidget(widgetType, widgetName[, ...]): widgetType: string expected (got "..type(widgetType)..")")
    ParserAssert(self, type(widgetName) == "string", "AddExtraWidget(widgetType, widgetName[, ...]): widgetName: string expected (got "..type(widgetName)..")")

    if self._wx.WindowSizer == nil then
        PrintWarning(self, "Layout not defined. Please define layout by using SetLayout() function.")
        error("Script parser stopped.")
    end

    if self._Flag[widgetName] == nil then
        self._Flag[widgetName] = {}
        self._Flag[widgetName].Value = ""
        self._Flag[widgetName].OrginalValue = ""

        GenericAddWidget(self, widgetType, widgetName, arg1, arg2, arg3, arg4)
    else
        PrintError(self, "Widget: '"..widgetName.."' is already defined")
    end
end

--------------------------------------------------------------------------------
-- @brief  Function read flag value in current file
-- @param  flagName         flag name
-- @param  filePath         external file (optional)
-- @return Flag value
--------------------------------------------------------------------------------
local function GetFlagValue(this, flagName, filePath)
    local self = this.self

    ParserAssert(self, type(flagName) == "string", "GetFlagValue(flagName[, filePath]): flagName: string expected (got "..type(flagName)..")")

    if type(filePath) == "string" then
        return KeyRead(filePath, flagName)
    else
        if flagName == "" then
            return nil
        elseif self._Flag[flagName] then
            return self._Flag[flagName].GetValue()
        else
            return nil
        end
    end
end

--------------------------------------------------------------------------------
-- @brief  Function get flag list according to regular expression
-- @param  filename         file where flags are read
-- @param  regexp           flag regular expression
-- @return Flag list (t[key] = value format).
--------------------------------------------------------------------------------
local function GetFlagsList(this, filename, regexp)
    local self = this.self

    ParserAssert(self, type(filename) == "string", "GetFlagsList(filename, regexp): filename: string expected (got "..type(filename)..")")
    ParserAssert(self, type(regexp) == "string", "GetFlagsList(filename, regexp): regexp: string expected (got "..type(regexp)..")")

    return KeyGetList(filename, regexp)
end

--------------------------------------------------------------------------------
-- @brief  Function set flag value in current file
-- @param  flagName         flag name
-- @param  value            value to set
-- @param  filePath         external file (optional)
-- @return None
--------------------------------------------------------------------------------
local function SetFlagValue(this, flagName, value, filePath)
    local self = this.self

    ParserAssert(self, type(flagName) == "string", "SetFlagValue(flagName, value[, filePath]): flagName: string expected (got "..type(flagName)..")")
    ParserAssert(self, type(value) == "string", "SetFlagValue(flagName, value[, filePath]): value: string expected (got "..type(value)..")")

    if type(filePath) == "string" then
        KeyWrite(filePath, flagName, value)
    else
        -- if function does not exists flag is not handled by widget
        if self._Flag[flagName].SetValue then
            self._Flag[flagName].SetValue(value)
        end
    end
end

--------------------------------------------------------------------------------
-- @brief  Function set tooltip for selected widget
-- @param  arg1             tip message/flag name
-- @param  arg2             tip message (if arg1 is flag name)
-- @return None
--------------------------------------------------------------------------------
local function SetToolTip(this, arg1, arg2)
    local self = this.self

    if type(arg1) == "string" and type(arg2) == "string" then
        local flagName   = arg1
        local tipMessage = arg2

        if self._Flag[flagName] ~= nil then
            self._Flag[flagName].SetToolTip(tipMessage)
        else
            PrintError(self, "SetToolTip(): flag '"..flagName.."' does not exists.")
        end

    elseif type(arg1) == "string" then
        local tipMessage = arg1

        if self._Flag[self._CurrentFlag] ~= nil then
            self._Flag[self._CurrentFlag].SetToolTip(tipMessage)
        else
            PrintError(self, "SetToolTip(): try to access to unknown flag.")
        end

    else
        PrintError(self, "Used: SetToolTip("..type(arg1)..", "..type(arg2).."): incorrect arguments, use:\n"..
                         "1. SetToolTip(string:TipMessage)\n"..
                         "2. SetToolTip(string:FlagName, string:TipMessage)\n")
    end
end

--------------------------------------------------------------------------------
-- @brief  Function add item to current widget (e.g. combobox, etc)
-- @param  itemName         item name (visible in widget)
-- @param  itemValue        item value (value stored in configuration file)
-- @param  flagName         flag name (optional)
-- @return None
--------------------------------------------------------------------------------
local function AddItem(this, itemName, itemValue, flagName)
    local self = this.self

    ParserAssert(self, type(itemName) == "string", "AddItem(itemName, itemValue): itemName: string expected (got "..type(itemName)..")")
    ParserAssert(self, type(itemValue) == "string", "AddItem(itemName, itemValue): itemValue: string expected (got "..type(itemValue)..")")

    if type(flagName) == "string" then
        if self._Flag[flagName] ~= nil then
            self._Flag[flagName].AddItem(itemName, itemValue)
        else
            PrintWarning(self, "AddItem(): flag '"..flagName.."' does not exists")
        end
    else
        self._Flag[self._CurrentFlag].AddItem(itemName, itemValue)
    end
end

--------------------------------------------------------------------------------
-- @brief  Function set event action of current widget
-- @param  event            event ("clicked", "PreSave", "PostSave", "PreDiscard", "PostDiscard", ...)
-- @param  func             function to handle event
-- @return None
--------------------------------------------------------------------------------
local function SetEvent(this, event, arg1, arg2)
    local self = this.self

    if event == "PreSave" or event == "PostSave"
    or event == "PreDiscard" or event == "PostDiscard" then
        self._Event[event] = arg1
    elseif event == "ConfigExport" then
        self._ExportEvent = arg1
    elseif event == "ConfigImport" then
        self._ImportEvent = arg1
    else
        if type(event) == "string" and type(arg1) == "string"  and type(arg2) == "function" then
            self._Flag[arg1].SetEvent(event, arg2)
        elseif type(event) == "string" and type(arg1) == "function" then
            self._Flag[self._CurrentFlag].SetEvent(event, arg1)
        else
            PrintError(self, "Function SetEvent() called with wrong arguments. Use as follow:\n"..
                             "1: SetEvent(eventName:string, widgetName:string, function)\n"..
                             "2: SetEvent(eventName:string, function)\n\n"..
                             "Events:\n"..
                             "clicked      - object clicked\n"..
                             "PreSave      - event is called before save procedure\n"..
                             "PostSave     - event is called after save procedure\n"..
                             "PreDiscard   - event is called before discard procedure\n"..
                             "PostDiscard  - event is called after discard procedure\n"..
                             "ConfigExport - event is called before configuration be exported\n"..
                             "ConfigImport - event is called before configuration be imported\n"
            )
        end
    end
end

--------------------------------------------------------------------------------
-- @brief  Function set widget as enabled or disabled
-- @param  en               enable state (true or false)
-- @param  flagName         flag name
-- @return None
--------------------------------------------------------------------------------
local function Enable(this, en, flagName)
    local self = this.self

    ParserAssert(self, type(en) == "boolean", "Enable(en): en: boolean expected (got "..type(en)..")")

    if type(flagName) == "string" then
        self._Flag[flagName].Enable(en)
    else
        self._Flag[self._CurrentFlag].Enable(en)
    end
end

--------------------------------------------------------------------------------
-- @brief  Function set configuration layout (must be called at beginning of configuration file)
-- @param  layoutName       layout name ("grid", )
-- @param  arg1             argument 1 (optional)
-- @param  arg2             argument 2 (optional)
-- @param  arg3             argument 3 (optional)
-- @param  arg4             argument 4 (optional)
-- @return None
--------------------------------------------------------------------------------
local function SetLayout(this, layoutName, arg1, arg2, arg3, arg4)
    local self = this.self

    if self._wx.WindowSizer == nil then
        if layoutName == "Grid" then
            ParserAssert(self, type(arg1) == "number", "SetLayout(\"Grid\", grids): grids: number expected (got "..type(arg1)..")")

            self._wx.WindowSizer = wx.wxFlexGridSizer(0, arg1, 0, 0)
            self._wx.ObjectSizer = self._wx.WindowSizer

        elseif layoutName == "TitledGrid" then
            ParserAssert(self, type(arg1) == "number", "SetLayout(\"TitledGrid\", grids, title): grids: number expected (got "..type(arg1)..")")
            ParserAssert(self, type(arg2) == "string", "SetLayout(\"TitledGrid\", grids, title): title: number expected (got "..type(arg2)..")")

            self._wx.WindowSizer = wx.wxFlexGridSizer(0, 1, 0, 0)

            local Label = wx.wxStaticText(self._wx.Window, wx.wxID_ANY, arg2.."")
            Label:SetFont(wx.wxFont(14, wx.wxDEFAULT, wx.wxFONTSTYLE_NORMAL, wx.wxBOLD))
            self._wx.WindowSizer:Add(Label, 0, (wx.wxALL + wx.wxEXPAND + wx.wxALIGN_LEFT + wx.wxALIGN_CENTER_VERTICAL), 5)

            self._wx.ObjectSizer = wx.wxFlexGridSizer(0, arg1, 0, 0)
            self._wx.WindowSizer:Add(self._wx.ObjectSizer, 0, (wx.wxALL + wx.wxEXPAND + wx.wxALIGN_LEFT + wx.wxALIGN_CENTER_VERTICAL), 2)

        elseif layoutName == "TitledGridBack" then
            ParserAssert(self, type(arg1) == "number", "SetLayout(\"TitledGridBack\", grids, title, func): grids: number expected (got "..type(arg1)..")")
            ParserAssert(self, type(arg2) == "string", "SetLayout(\"TitledGridBack\", grids, title, func): title: string expected (got "..type(arg2)..")")

            self._wx.WindowSizer = wx.wxFlexGridSizer(0, 1, 0, 0)

            -- button and title label
            self._wx.SizerTitle = wx.wxFlexGridSizer(0, 2, 0, 0)
            self._wx.SizerTitle.ButtonId = wx.wxNewId()
            self._wx.SizerTitle.Button   = wx.wxBitmapButton(self._wx.Window,
                                                             self._wx.SizerTitle.ButtonId,
                                                             icon.px22.go_back)
            self._wx.SizerTitle.Button:SetBitmapDisabled(icon.px22.go_back_dimmed)
            if type(arg3) ~= "function" then
                self._wx.SizerTitle.Button:Enable(false)
            else
                self._wx.Window:Connect(self._wx.SizerTitle.ButtonId, wx.wxEVT_COMMAND_BUTTON_CLICKED, arg3)
            end

            self._wx.SizerTitle:Add(self._wx.SizerTitle.Button, 0, (wx.wxALL + wx.wxALIGN_LEFT + wx.wxALIGN_CENTER_VERTICAL), 5)

            self._wx.SizerTitle.Label = wx.wxStaticText(self._wx.Window, wx.wxID_ANY, arg2.."")
            self._wx.SizerTitle.Label:SetFont(wx.wxFont(14, wx.wxDEFAULT, wx.wxFONTSTYLE_NORMAL, wx.wxBOLD))
            self._wx.SizerTitle:Add(self._wx.SizerTitle.Label, 0, (wx.wxALL + wx.wxALIGN_LEFT + wx.wxALIGN_CENTER_VERTICAL), 5)

            -- object sizer
            self._wx.ObjectSizer = wx.wxFlexGridSizer(0, arg1, 0, 0)
            self._wx.WindowSizer:Add(self._wx.SizerTitle, 0, (wx.wxALL + wx.wxEXPAND + wx.wxALIGN_LEFT + wx.wxALIGN_CENTER_VERTICAL), 2)
            self._wx.WindowSizer:Add(self._wx.ObjectSizer, 0, (wx.wxALL + wx.wxEXPAND + wx.wxALIGN_LEFT + wx.wxALIGN_CENTER_VERTICAL), 2)

        else
            PrintError(self, "Unknown layout: "..layoutName.."\n"..
                             "Available layouts:\n"..
                             "  Grid(grids)\n"..
                             "  TitledGrid(grids, title)\n"..
                             "  TitledGridBack(grids, title[, func])\n"
            )
        end
    else
        PrintWarning(self, "SetLayout(): layout already defined")
    end
end

--------------------------------------------------------------------------------
-- @brief  Function load a new file (start interpreter)
-- @param  this
-- @param  fileName
-- @param  save         optional, if false configuration is not saved
-- @return None
--------------------------------------------------------------------------------
local function LoadFile(this, fileName, save)
    local self = this.self

    ParserAssert(self, type(fileName) == "string", "LoadFile(fileName): fileName: string expected (got "..type(fileName)..")")

    if self:IsModified() then
        local answer = ShowQuestionMessage(MAIN_WINDOW_NAME,
                                           SAVE_QUESTION,
                                           iff(save == false,
                                              (wx.wxNO + wx.wxCANCEL),
                                              (wx.wxYES_NO + wx.wxCANCEL)),
                                           self._ParentWindow)
        if answer == wx.wxID_CANCEL then
            return
        elseif answer == wx.wxID_YES and save ~= false then
            self:Save()
        end
    end

    self:Parse(fileName)
end

--============================================================================--
-- PARSER CLASS FUNCTIONS
--============================================================================--
--------------------------------------------------------------------------------
-- @brief  Method parse selected file
-- @param  self
-- @param  fileName
-- @return true on success, otherwise false
--------------------------------------------------------------------------------
local function Parse(self, fileName)
    ui.Frame:SetStatusText("Loading configuration...", 1)

    self._ParentWindow:Freeze()
    self._FileName = iff(type(fileName) == "string", fileName, self._FileName)

    if self._wx.WindowSizer ~= nil then
        self._wx.Window:Freeze()
        self._wx.WindowSizer:Clear(true)
        self._wx.WindowSizer = nil

        self._CurrentFlag    = ""
        self._ChunkBegin     = 1
        self._ChunkEnd       = 1
        self._Event          = {}
        self._Flag           = {}
    end

    RunParser(self)

    if self._wx.WindowSizer ~= nil then
        self._wx.Window:SetSizer(self._wx.WindowSizer, true)
        self._wx.Window:SetVirtualSize(self._ParentWindow:GetSize())
        self._wx.Window:SetScrollRate(15, 15)
    end

    -- refreshes window content (by resising window by 1 px)
    local WSIZE = ui.Frame:GetSize() WSIZE:DecBy(1, 0)
    self._ParentWindow:SetSize(WSIZE) WSIZE:IncBy(1, 0)
    self._ParentWindow:SetSize(WSIZE)

    self._wx.Window:Thaw()
    self._ParentWindow:Thaw()

    ui.Frame:SetStatusText("", 1)
end

--------------------------------------------------------------------------------
-- @brief  Method save all values
-- @param  self
-- @return true on success, otherwise false
--------------------------------------------------------------------------------
local function Save(self)
    if CallEvent(self, "PreSave") == false then
        return false
    end

    for key, val in pairs(self._Flag) do
        if key:match("("..self._KeyRegExp..")") then
            if val.OrginalValue ~= val.Value then
                KeyWrite(self._FileName, key, val.Value)
                val.OrginalValue = val.Value
            end
        end
    end

    CallEvent(self, "PostSave")

    return true
end

--------------------------------------------------------------------------------
-- @brief  Method discard values
-- @param  self
-- @return None
--------------------------------------------------------------------------------
local function Discard(self)
    CallEvent(self, "PreDiscard")

    for key, val in pairs(self._Flag) do
        if key:match("("..self._KeyRegExp..")") then
            if val.OrginalValue ~= val.Value then
                SetFlagValue(self._script, key, val.OrginalValue)
            end
        end
    end

    CallEvent(self, "PostDiscard")
end

--------------------------------------------------------------------------------
-- @brief  Method check if values are modified
-- @param  self
-- @return true on success, otherwise false
--------------------------------------------------------------------------------
local function IsModified(self)
    for key, val in pairs(self._Flag) do
        if key:match("("..self._KeyRegExp..")") then
            if val.OrginalValue ~= val.Value then
                return true
            end
        end
    end

    return false
end

--------------------------------------------------------------------------------
-- @brief  Method call selected event
-- @param  self
-- @param  eventName
-- @return true on success, otherwise false
--------------------------------------------------------------------------------
local function CallEvent(self, eventName)
    if eventName == "ConfigExport" and type(self._ExportEvent) == "function" then
        self._ExportEvent()
    elseif eventName == "ConfigImport" and type(self._ImportEvent) == "function" then
        self._ImportEvent()
    else
        print(GetErrorMsg(self, "Unknown event or function not declared"))
    end
end

--------------------------------------------------------------------------------
-- @brief  Function create new configuration parser
-- @param  None
-- @return None
--------------------------------------------------------------------------------
function CreateParser(parentWindow, fileName)
    local self = {}

    -- class objects
    self._KeyRegExp     = "[a-zA-Z0-9_]+"
    self._CurrentFlag   = ""
    self._ChunkBegin    = 1
    self._ChunkEnd      = 1
    self._FileName      = fileName
    self._ParentWindow  = parentWindow
    self._wx            = {}
    self._Event         = {}
    self._Flag          = {}
    self._ImportEvent   = function() end
    self._ExportEvent   = function() end

    if parentWindow ~= nil then
        self._wx.Window = wx.wxScrolledWindow(self._ParentWindow, wx.wxID_ANY)
    end

    -- script functions
    self._script                = {}
    self._script.self           = self
    self._script.finish         = false
    self._script.AddWidget      = AddWidget
    self._script.SetLayout      = SetLayout
    self._script.SetToolTip     = SetToolTip
    self._script.GetFlagValue   = GetFlagValue
    self._script.SetFlagValue   = SetFlagValue
    self._script.GetFlagsList   = GetFlagsList
    self._script.AddItem        = AddItem
    self._script.SetEvent       = SetEvent
    self._script.AddExtraWidget = AddExtraWidget
    self._script.LoadFile       = LoadFile
    self._script.Enable         = Enable
    self._script.Finish         = function() self._script.finish = true end
    self._script.Reload         = function() LoadFile(self._script, self._FileName) end
    self._script.Save           = function() self:Save() end
    self._script.SetKeyRegExp   = function(_, regExp) self._KeyRegExp = regExp end

    -- methodes
    self.Parse        = Parse
    self.GetWindow    = function(self) return self._wx.Window end
    self.Save         = Save
    self.Discard      = Discard
    self.IsModified   = IsModified
    self.IsParsed     = function(self) return self._wx.WindowSizer ~= nil end
    self.GetFileName  = function(self) return self._FileName end
    self.CallEvent    = CallEvent

    return self
end
