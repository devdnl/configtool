Plugins are files written in lua script. Script must has .lua file extension to
be loaded.

To change plugins directory to another enter argument in the command line as folllow:

   ./configtool <configuration file> PLUGINS-DIR=./my/plugin/dir

